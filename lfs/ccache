###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  IPFire Team  <info@ipfire.org>                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VER        = 3.1.6

THISAPP    = ccache-$(VER)
DL_FILE    = $(THISAPP).tar.bz2
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)-pass$(PASS)

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE)

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_MD5 = 343dc9b642e1d2af1e6bd8e474dde92e

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar jxf $(DIR_DL)/$(DL_FILE)
	cd $(DIR_APP) && patch -p1 < $(DIR_SRC)/src/patches/$(THISAPP)-no_compiler_mtime.patch
	cd $(DIR_APP) && CFLAGS="-O2 -static -DDEFAULT_MAXSIZE=2097152" ./configure --prefix=$(ROOT)/usr/local
	cd $(DIR_APP) && make $(MAKETUNING) $(EXTRA_MAKE)
	cd $(DIR_APP) && make install
	ln -sf ccache $(ROOT)/usr/local/bin/gcc
	ln -sf ccache $(ROOT)/usr/local/bin/g++
	ln -sf ccache $(ROOT)/usr/local/bin/cc
	ln -sf ccache $(ROOT)/usr/local/bin/$(BUILDTARGET)-gcc
	ln -sf ccache $(ROOT)/usr/local/bin/$(BUILDTARGET)-g++
	ln -sf ccache $(ROOT)/usr/local/bin/$(BUILDTARGET)-cc
	$(ROOT)/usr/local/bin/ccache -z
	@rm -rf $(DIR_APP)
	@$(POSTBUILD)
