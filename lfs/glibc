###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2011  IPFire Team  <info@ipfire.org>                     #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

ifeq "$(MACHINE_TYPE)" "arm"
	VER = 2.5
else
	VER = 2.3.6
endif

THISAPP    = glibc-$(VER)
DL_FILE    = $(THISAPP).tar.bz2
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)

# Normal build or /tools build.
#
ifeq "$(ROOT)" ""
  TARGET = $(DIR_INFO)/$(THISAPP)
  EXTRA_CONFIG = --prefix=/usr \
    --disable-profile --enable-add-ons \
    --enable-kernel=2.6.0 --libexecdir=/usr/lib/glibc
  EXTRA_MAKE =
  EXTRA_INSTALL =
else
  TARGET = $(DIR_INFO)/$(THISAPP)-tools
  EXTRA_CONFIG = --prefix=/tools \
    --disable-profile --enable-add-ons \
    --enable-kernel=2.6.0 --with-binutils=/tools/bin \
    --without-gd --with-headers=/tools/include \
    --without-selinux
  EXTRA_MAKE =
  EXTRA_INSTALL =
endif

EXTRA_CONFIG += --build=$(BUILDTARGET)

ifeq "$(MACHINE_TYPE)" "arm"
	# Disable hardware FP for ARM.
	EXTRA_CONFIG += \
		--without-fp
endif

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE) \
	glibc-libidn-$(VER).tar.bz2

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)
glibc-libidn-$(VER).tar.bz2 = $(DL_FROM)/glibc-libidn-$(VER).tar.bz2

ifeq "$(MACHINE_TYPE)" "arm"
	$(DL_FILE)_MD5 = 1fb29764a6a650a4d5b409dda227ac9f
	glibc-libidn-$(VER).tar.bz2_MD5 = 8787868ba8962d9b125997ec2f25ac01
else
	$(DL_FILE)_MD5 = bfdce99f82d6dbcb64b7f11c05d6bc96
	glibc-libidn-$(VER).tar.bz2_MD5 = 49dbe06ce830fc73874d6b38bdc5b4db
endif

# ARM needs glibc-ports
ifeq "$(MACHINE_TYPE)" "arm"
	objects += glibc-ports-$(VER).tar.bz2
	glibc-ports-$(VER).tar.bz2 = $(DL_FROM)/glibc-ports-$(VER).tar.bz2
	glibc-ports-$(VER).tar.bz2_MD5 = 183f6d46e8fa5e4b2aff240ab1586c2e
endif

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) $(DIR_SRC)/glibc-build && cd $(DIR_SRC) && tar jxf $(DIR_DL)/$(DL_FILE)
	@mkdir $(DIR_SRC)/glibc-build
ifeq "$(MACHINE_TYPE)" "arm"
	cd $(DIR_APP) && tar jxf $(DIR_DL)/glibc-ports-$(VER).tar.bz2
	cd $(DIR_APP) && mv -v glibc-ports-$(VER) ports

	cd $(DIR_APP)/ports && patch -Np1 -i $(DIR_SRC)/src/patches/glibc-ports-avoid-using-asm-procinfo.patch

	# asm/page.h should not be included in sysdeps/unix/sysv/linux/arm/ioperm.c.
	cd $(DIR_APP) && sed "/asm\/page.h/d" -i ports/sysdeps/unix/sysv/linux/arm/ioperm.c
else
	cd $(DIR_APP) && patch -Np1 -i $(DIR_SRC)/src/patches/$(THISAPP)-dont_use_origin_on_privil_exec.patch
endif

ifeq "$(ROOT)" ""
ifeq "$(MACHINE)" "i586"
	cd $(DIR_APP) && patch -Np1 -i $(DIR_SRC)/src/patches/$(THISAPP)-linux_types-1.patch
	cd $(DIR_APP) && patch -Np1 -i $(DIR_SRC)/src/patches/$(THISAPP)-inotify-1.patch
endif
	# This locale causes a loop on bash login - exclude it
	cd $(DIR_APP) && sed -i '/vi_VN.TCVN/d' localedata/SUPPORTED
ifeq "$(MACHINE_TYPE)" "arm"
	cd $(DIR_APP) && sed -i 's|libs -o|libs -L/usr/lib -Wl,-dynamic-linker=/lib/ld-linux.so.3 -o|' \
		scripts/test-installation.pl
else
	cd $(DIR_APP) && sed -i 's|libs -o|libs -L/usr/lib -Wl,-dynamic-linker=/lib/ld-linux.so.2 -o|' \
		scripts/test-installation.pl
endif
endif
	cd $(DIR_SRC)/glibc-build && CFLAGS="$(CFLAGS)" $(DIR_APP)/configure $(EXTRA_CONFIG)

ifeq "$(ROOT)" ""
	touch /etc/ld.so.conf
	cd $(DIR_SRC)/glibc-build && make $(MAKETUNING) $(EXTRA_MAKE)
	cd $(DIR_SRC)/glibc-build && make $(EXTRA_INSTALL) install
ifeq "$(MACHINE)" "i586"
	cp -v $(DIR_APP)/sysdeps/unix/sysv/linux/inotify.h /usr/include/sys
endif
else
	-mkdir /tools/etc
	touch /tools/etc/ld.so.conf
	cd $(DIR_SRC)/glibc-build && make $(MAKETUNING) $(EXTRA_MAKE)
	cd $(DIR_SRC)/glibc-build && make $(EXTRA_INSTALL) install
endif

	# Creating the locales
ifeq "$(ROOT)" ""
	mkdir -p /usr/lib/locale
	cd $(DIR_SRC)/glibc-build && localedef -i en_US -f ISO-8859-1 en_US
	cd $(DIR_SRC)/glibc-build && localedef -i en_US -f UTF-8      en_US.utf8
endif
	@rm -rf $(DIR_APP) $(DIR_SRC)/glibc-build
	@$(POSTBUILD)
